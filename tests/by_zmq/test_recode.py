#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '16/1/10'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


import zmq
import time
import signal
import thread


# Prepare our context and sockets
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('tcp://localhost:3000')

exit_flag = False
counter = 0
cycle_rate = 0
message = dict()
analyze_raw = []
analyze_slow = []
analyze_result = []


def signal_handle(signum=0, frame=None):
    global exit_flag
    exit_flag = True

signal.signal(signal.SIGTERM, signal_handle)
signal.signal(signal.SIGINT, signal_handle)


def sql_exec():
    global cycle_rate, exit_flag
    while not exit_flag:
        cycle_rate += 1
        message['action'] = 'RPC'
        message['object_name'] = 'order_form'
        message['parms'] = {
            'function': 'generate_id_by',
        }
        socket.send_json(message)
        result = socket.recv_json()

        message['action'] = 'POST'
        message['object_name'] = 'order_form'
        message['parms'] = {
            'id': result['id'],
            'create_time': 0,
            'finished_time': 0
        }
        socket.send_json(message)
        socket.recv_json()


def monitor_sql_exec():
    global counter, cycle_rate, exit_flag
    used_time = 0
    while True:
        if exit_flag:
            return
        time.sleep(1)
        used_time += 1
        counter += cycle_rate
        analyze_raw.append(cycle_rate)
        print ''.join(['Used time: ', str(used_time), ' mark: ', str(counter), ', rate: ', str(cycle_rate)])
        cycle_rate = 0


start_time = time.asctime()
thread.start_new_thread(sql_exec, ())
thread.start_new_thread(monitor_sql_exec, ())

while not exit_flag:
    time.sleep(1)

end_time = time.asctime()
print analyze_raw
print ''.join(['Start time: ', start_time])
print ''.join(['End time: ', end_time])
