#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/12/16'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


own_state_branch = {
    # ----------以上将集成至jimit库中------------
    '41250': {
        'code': '41250',
        'zh-cn': u'未支持索引的字段'
    },
    '50050': {
        'code': '50150',
        'zh-cn': u'MySQL 链接或执行出错'
    },
    '50051': {
        'code': '50051',
        'zh-cn': u'Redis 链接或执行出错'
    },
}
