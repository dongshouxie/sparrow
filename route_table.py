#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/12/20'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

from models.utils import add_rule
from views import recode_operate

# 记录操作相关蓝图
add_rule(recode_operate.blueprint, '/<name>', view_func='recode_operate.r_record_create', methods=['POST'])
add_rule(recode_operate.blueprint, '/<name>', view_func='recode_operate.r_record_get', methods=['GET'])
add_rule(recode_operate.blueprint, '/<name>/<_id>', view_func='recode_operate.r_record_update', methods=['PATCH'])
add_rule(recode_operate.blueprint, '/<name>/<_id>', view_func='recode_operate.r_record_delete', methods=['DELETE'])
